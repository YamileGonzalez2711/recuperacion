<?php

namespace crud;

class consultas
{
    public $nombre, $caducidad, $descripcion, $cantidad;

    public function registrar()
    {
        $conexion = new conexion();
        $consulta = mysqli_prepare($conexion->con, "INSERT INTO inventario (NOMBRE,CADUCIDAD,DESCRIPCION,CANTIDAD) VALUES (?,?,?,?)");
        $consulta->bind_param("ssss", $this->nombre, $this->caducidad, $this->descripcion, $this->cantidad);
        $consulta->execute();
        ?>
        <script>
            window.location = "http://localhost/YamileGonzalez/index.php?controller=crud&action=tabla";
        </script>
        <?php
    }
    public function eliminar($id)
    {
        $conexion = new conexion();
        $consulta = mysqli_prepare($conexion->con, "DELETE FROM inventario WHERE ID = ?");
        $consulta->bind_param("s", $id);
        $consulta->execute();
        ?>
        <script>
            window.location = "http://localhost/YamileGonzalez/index.php?controller=crud&action=tabla";
        </script>
        <?php
    }
    public function actualizar($id){
        $conexion = new conexion();
        $consulta = mysqli_prepare($conexion->con,"UPDATE inventario SET NOMBRE = ?, CADUCIDAD = ?, DESCRIPCION = ?, CANTIDAD = ? WHERE ID = ?");
        $consulta->bind_param("sssss", $this->nombre, $this->caducidad, $this->descripcion, $this->cantidad,$id);
        $consulta->execute();
        ?>
        <script>
            window.location = "http://localhost/YamileGonzalez/index.php?controller=crud&action=tabla";
        </script>
        <?php
    }
    public function entrefechas($fecha1,$fecha2){
        $conexion = new conexion();
        $consulta = "SELECT * FROM inventario WHERE CADUCIDAD BETWEEN '$fecha1' AND '$fecha2'";
        $resultado = mysqli_query($conexion->con,$consulta);
        ?>
 <table >
                <tr >
                    <th >ID</th>
                    <th >Nombre</th>
                    <th >Caducidad</th>
                    <th >Descripcion</th>
                    <th >Cantidad</th>
                </tr>
     <?php
        while ($filas = mysqli_fetch_array($resultado)){?>
            <tr>
                <td >
                    <?php echo $filas[0]; ?>
                </td>
                <td >
                    <?php echo $filas[1]; ?>
                <td >
                    <?php echo $filas[2]; ?>
                </td>
                <td >
                    <?php echo $filas[3]; ?>
                </td>
                <td >
                    <?php echo $filas[4]; ?>
                </td>
            </tr>
            <?php
        }
    }
    public function Todo(){
        $conexion = new conexion();
        $consulta = "SELECT * FROM inventario";
        $resultado = mysqli_query($conexion->con,$consulta);
        ?>
            <form method="POST" action="http://localhost/YamileGonzalez/index.php?controller=crud&action=crearTarea">
                <table class="table-registro">
                    <tr>
                        <th>NOMBRE</th>
                        <th>CADUCIDAD</th>
                        <th>DESCRIPCION</th>
                        <th>CANTIDAD</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="nombre" class="centrar-texto" placeholder="nombre" required>
                        <td>
                            <input type="date" name="caducidad" class="centrar-texto" placeholder="caducidad" required>
                        </td>
                        <td>
                            <input type="text" name="descripcion" class="centrar-texto" value="">
                        </td>
                        <td>
                            <input type="number" name="cantidad" class="centrar-texto" required>
                        </td>
                    </tr>
                </table>
                <input type="submit"  value="Agregar nuevo" class="boton">
            </form>
            <table >
                <tr >
                    <th >ID</th>
                    <th >Nombre</th>
                    <th >Caducidad</th>
                    <th >Descripcion</th>
                    <th >Cantidad</th>
                </tr>
                <?php
                while ($filas = mysqli_fetch_array($resultado)){?>
                    <tr>
                        <td>
                            <?php echo $filas[0]; ?>
                        </td>
                        <td>
                            <?php echo $filas[1]; ?>
                        <td>
                            <?php echo $filas[2]; ?>
                        </td>
                        <td>
                            <?php echo $filas[3]; ?>
                        </td>
                        <td>
                            <?php echo $filas[4]; ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <h3>Actualizar registro</h3>
            <form method="POST" action="http://localhost/YamileGonzalez/index.php?controller=crud&action=actualizar">
                <label>Ingrese el ID a editar</label>
                <input type="number" size="3" name="id_ac">
                <table>
                    <tr>
                        <th>Nombre</th>
                        <th>caducidad</th>
                        <th>descripcion</th>
                        <th>cantidad</th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="nombre_ac" class="centrar-texto" placeholder="Titulo" required>
                        <td>
                            <input type="date" name="caducidad_ac" class="centrar-texto"  required>
                        </td>
                        <td>
                            <input type="text" name="descripcion_ac" class="centrar-texto" value="" >
                        </td>
                        <td>
                            <input type="number" name="cantidad_ac" value="">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="editar" class="boton-rojo"><br>
            </form>
            <h3>Eliminar registro</h3>
            <form  method="POST" action="http://localhost/crudYamile/index.php?controller=crud&action=eliminar">
                <label>Ingrese el ID a eliminar</label>
                <input type="number"size="3" name="id_el">
                <br>
                <input type="submit" value="eliminar" class="boton-rojo">
            </form>
            <form method="POST" action="http://localhost/crudYamile/index.php?controller=crud&action=consultar">
                <h1>Consultar entre fechas</h1>
                <label>Fecha 1</label>
                <input type="date" name="fecha1" required>
                <label>Fecha 2</label>
                <input type="date" name="fecha2" required>
                <input type="submit" value="Consultar">
            </form>
        <?php
    }
}
?>

