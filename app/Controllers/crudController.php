<?php

require "app/Models/consultas.php";
require "app/Models/conexion.php";

use crud\conexion;
use crud\consultas;

class crudController
{
    public function tabla(){
        $model = new consultas();
        $model -> Todo();

    }
    public function crearTarea(){
        $model = new consultas();
        $model -> nombre = $_POST["nombre"];
        $model -> caducidad = $_POST["caducidad"];
        $model -> descripcion = $_POST["descripcion"];
        $model -> cantidad = $_POST["cantidad"];
        $model -> registrar();

    }
    public function actualizar(){
        $model = new consultas();
        $model -> nombre = $_POST["nombre_ac"];
        $model -> caducidad = $_POST["caducidad_ac"];
        $model -> descripcion = $_POST["descripcion_ac"];
        $model -> cantidad = $_POST["cantidad_ac"];
        $model -> actualizar($_POST["id_ac"]);

    }
    public function eliminar(){
        $model = new consultas();
        $model -> eliminar($_POST["id_el"]);

    }
    public function consultar(){
        $model = new consultas();
        $model->entrefechas($_POST["fecha1"],$_POST["fecha2"]);
    }
}